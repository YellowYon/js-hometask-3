# hometask-10

## Ответьте на вопросы

#### 1. Как можно сменить контекст вызова? Перечислите все способы и их отличия друг от друга.

> Ответ: bind - привязывает контекст к функции, таким образом все последующие вызовы будут иметь новый контекст. apply/call - вызывают функцию "на месте" с переданными через запятую/массивом аргументами.

#### 2. Что такое стрелочная функция?

> Ответ: это функция, которая не имеет своего this и имеет следующий синтаксис (args)=>{}; Из этого следует, что такие функции не подходят в качестве методов объектов, с ними не работает bind/call/apply,не могут быть конструкторами

#### 3. Приведите свой пример конструктора.

```js
function Cat(name, age) {
  this.name = name;
  this.age = age;
}

const Boris = new Cat('Boris', 10);
```

#### 4. Исправьте код так, чтобы в `this` попадал нужный контекст. Исправить нужно 3мя способами, как показано в примерах урока.

```js
//bind
const person = {
  name: 'Nikita',
  sayHello: function () {
    setTimeout(
      function () {
        console.log(this.name + ' says hello to everyone!');
      }.bind(this),
      1000
    );
  },
};
//that
const person = {
  name: 'Nikita',
  sayHello: function () {
    const that = this;
    setTimeout(function () {
      console.log(that.name + ' says hello to everyone!');
    }, 1000);
  },
};
//arrow
const person = {
  name: 'Nikita',
  sayHello: function () {
    setTimeout(() => {
      console.log(this.name + ' says hello to everyone!');
    }, 1000);
  },
};
person.sayHello();
```

## Выполните задания

- Установите зависимости `npm install`;
- Допишите функции в `task.js`;
- Проверяйте себя при помощи тестов `npm run test`;
- Создайте Merge Request с решением.
