const { CreateColor, useColor, Song } = require('./task');

describe('CreateColor', () => {
  test('должен возвращать объект', () => {
    expect(typeof new CreateColor('')).toBe('object')
  });
  test('в вернувшемся объекте должно быть поле с названием цвета', () => {
    const redColor = new CreateColor('красный');
    expect(redColor.colorName).toBe('красный')
  });
  test('метод use должен возвращать строку про использование', () => {
    const redColor = new CreateColor('красный');
    expect(redColor.use()).toBe('Используется красный цвет')
  });
  test('метод stopUse должен возвращать строку про окончание использования', () => {
    const yellowColor = new CreateColor('желтый');
    expect(yellowColor.stopUse()).toBe('Желтый цвет больше не используется')
  });
  test('метод use должен корректно работать при вызове в контексте другого объекта', () => {
    const yellowColor = new CreateColor('желтый');
    const blackColor = {
      colorName: 'черный',
      use: yellowColor.use,
    };
    expect(blackColor.use()).toBe('Используется черный цвет')
  });
  test('метод stopUse должен корректно работать при вызове в контексте другого объекта', () => {
    const yellowColor = new CreateColor('желтый');
    expect(yellowColor.stopUse.call({ colorName: 'голубой' })).toBe('Голубой цвет больше не используется')
  });
  test('метод use должен приводить название к нижнему регистру', () => {
    const yellowColor = new CreateColor('жЕлТыЙ');
    expect(yellowColor.use()).toBe('Используется желтый цвет')
  });
  test('метод stopUse должен приводить название к нижнему регистру, а первую букву делать заглавной', () => {
    const greenColor = new CreateColor('зеЛеный');
    expect(greenColor.stopUse()).toBe('Зеленый цвет больше не используется')
  });
});

describe('useColor', () => {
  test('метод useColor1 должен возвращать строку про использование', () => {
    const result = useColor.useColor1();
    expect(result).toBe('Используется серо-буро-малиновый в крапинку цвет');
  });
  test('метод useColor2 должен возвращать строку про использование', () => {
    const result = useColor.useColor2();
    expect(result).toBe('Используется серо-буро-малиновый в крапинку цвет');
  });
  test('метод useColor3 должен возвращать строку про использование', () => {
    const result = useColor.useColor3();
    expect(result).toBe('Используется серо-буро-малиновый в крапинку цвет');
  });
});


describe('Song', () => {
  test('должен возвращать объект', () => {
    expect(typeof new Song('', '', '')).toBe('object')
  });
  test('в вернувшемся объекте должно быть поля title, author и album', () => {
    const song = new Song('title', 'author', 'album');
    expect(song.title).toBe('title');
    expect(song.author).toBe('author');
    expect(song.album).toBe('album');
    expect(song.year).toBe(undefined);
  });
  test('метод getFullName должен возвращать отформатированную строку', () => {
    const song = new Song('title', 'author', 'album');
    expect(song.getFullName()).toBe("композиция «title», исполнитель author, альбом «album»");
  });
  test('метод setYear должен должен добавлять поле year к объекту', () => {
    const song = new Song('title', 'author', 'album');
    song.setYear(2019);
    expect(song.year).toBe(2019);
  });
  test('метод setYear должен сохранять год as number', () => {
    const song = new Song('title', 'author', 'album');
    song.setYear('2019');
    expect(song.year).toBe(2019);
  });
  test('метод setTitle должен изменять поле title у объекта', () => {
    const song = new Song('title', 'author', 'album');
    song.setTitle('title2');
    expect(song.title).toBe('title2');
    expect(song.getFullName()).toBe("композиция «title2», исполнитель author, альбом «album»");
  });
})
